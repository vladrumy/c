#pragma once

#include <memory>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>

std::vector<std::shared_ptr<std::string>> DeDuplicate(const std::vector<std::unique_ptr<std::string>>& items) {
    std::vector<std::shared_ptr<std::string>> deduplicated;
    std::unordered_map<std::string, std::shared_ptr<std::string>> uniqueStrings;
    for (const auto& item : items) {
        auto it = uniqueStrings.find(*item);
        if (it == uniqueStrings.end()) {
            std::shared_ptr<std::string> sharedStr = std::make_shared<std::string>(*item);
            deduplicated.push_back(sharedStr);
            uniqueStrings[*item] = sharedStr;
        } else {
            deduplicated.push_back(it->second);
        }
    }
    return deduplicated;
}

std::vector<std::unique_ptr<std::string>> Duplicate(const std::vector<std::shared_ptr<std::string>>& items) {
    std::vector<std::unique_ptr<std::string>> duplicated;
    for (const auto& item : items) {
        std::unique_ptr<std::string> uniqueStr = std::make_unique<std::string>(*item);
        duplicated.push_back(std::move(uniqueStr));
    }
    return duplicated;
}
