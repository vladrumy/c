#pragma once

#include <string>
#include <iostream>

using std::cout;

class WeakPtr;

struct Counters {
    int strong = 0;
    int weak = 0;

    Counters() {};

    Counters(int strong, int weak) :
        strong(strong)
        , weak(weak)
    {}
};

class SharedPtr {
private:
    Counters* counter_;
    std::string* obj_;

public:
    SharedPtr() :
        obj_(nullptr)
        , counter_(new Counters(1, 0))
    {}

    SharedPtr(std::string* ptr) :
        obj_(ptr)
        , counter_(new Counters(1, 0))
    {}

    SharedPtr(const SharedPtr& other) :
        obj_(other.obj_)
        , counter_(other.counter_)
    {
        ++counter_->strong;
    }

    SharedPtr(SharedPtr&& other) :
        counter_(other.counter_)
        , obj_(other.obj_)
    {
        other.obj_ = nullptr;
        other.counter_ = nullptr;
    }

    SharedPtr(const WeakPtr& other);

    SharedPtr& operator=(SharedPtr other) {
        Swap(other);
        return *this;
    }

    void Swap(SharedPtr& other) {
        std::swap(counter_, other.counter_);
        std::swap(obj_, other.obj_);
    }

    void Reset(std::string* other) {
        if (--counter_->strong == 0) {
            delete obj_;
            if (counter_->strong + counter_->weak == 0) {
                delete counter_;
            }
        }
        obj_ = other;
        counter_ = new Counters{1, 0};
    }

    ~SharedPtr() {
        if (counter_ == nullptr) {
            return;
        }
        if (--counter_->strong == 0) {
            delete obj_;
        }
        if (counter_->strong + counter_->weak == 0) {
            delete counter_;
        }
    }

    std::string* Get() const {
        return obj_;
    }

    Counters* GetCounters() const {
        return counter_;
    }

    std::string operator*() {
        return *obj_;
    }

    std::string* operator->() {
        return obj_;
    }

    friend class WeakPtr;
};

class WeakPtr {
private:
    std::string* obj_;
    Counters* counter_;

public:
    WeakPtr() :
        obj_(nullptr)
        , counter_(new Counters(0, 1))
    {}

    WeakPtr(const WeakPtr& other) :
        obj_(other.obj_)
        , counter_(other.counter_)
    {
        if (counter_ != nullptr) {
            ++counter_->weak;
        }
    }

    WeakPtr(const SharedPtr& other) :
          obj_(other.Get())
          , counter_(other.GetCounters())
    {
        ++counter_->weak;
    }

    WeakPtr(WeakPtr&& other) :
        obj_(other.obj_)
        , counter_(other.counter_)
    {
        other.obj_ = nullptr;
        other.counter_ = nullptr;
    }

    WeakPtr& operator=(WeakPtr other) {
        Swap(other);
        return *this;
    }

    void Swap(WeakPtr& other) {
        std::swap(counter_, other.counter_);
        std::swap(obj_, other.obj_);
    }

    bool IsExpired() const {
        return counter_->strong == 0;
    }

    SharedPtr Lock() {
        if (IsExpired()) {
            return SharedPtr();
        }
        return SharedPtr(*this);
    }

    std::string* Get() const {
        return obj_;
    }

    Counters* GetCounters() const {
        return counter_;
    }

    ~WeakPtr() {
        if (counter_ == nullptr) {
            return;
        }
        --counter_->weak;
        if (counter_->weak + counter_->strong == 0) {
            delete counter_;
        }
    }
};

SharedPtr::SharedPtr(const WeakPtr& other) :
    counter_(other.GetCounters())
{
    if (other.IsExpired()) {
        obj_ = nullptr;
    } else {
        obj_ = other.Get();
    }
    ++counter_->strong;
}
