#pragma once

#include <string>
#include <stdexcept>
#include <unordered_set>

std::string Lower(std::string string) {
    for (int i = 0; i < string.size(); ++i) {
        string[i] = tolower(string[i]);
    }
}

int DifferentWordsCount(const std::string& string) {
    std::unordered_set<std::string> words;
    int begin, length;
    for (auto ch : string) {
        if (isalpha(ch)) {
            ++length;
        } else {
            words.insert(Lower(string.substr(begin, length)));
            length = 0;
        }
        ++begin;
    }
    if (length > 0) {
        words.insert(Lower(string.substr(begin, length)));
    }
    return words.size();
}
