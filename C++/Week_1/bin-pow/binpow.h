#pragma once

#include <stdexcept>

int BinPow(int a, int64_t b, int c) {
    auto a_ = static_cast<int64_t>(a);
    int64_t ans = 1;
    while (b) {
        if (b % 2) {
            ans = ans * a_ % c;
        }
        a_ = a_ * a_ % c;
        b /= 2;
    }
    return static_cast<int>(ans);
}
