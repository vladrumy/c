#pragma once

#include <vector>
#include <stdexcept>


template <typename T> int Sign(T val) {
    return (T(0) < val) - (val < T(0));
}


std::vector<int> Range(int from, int to, int step = 1) {
    std::vector<int> ans;
    auto cur = from;
    while (Sign(step) * cur < Sign(step) * to) {
        ans.push_back(cur);
        cur += step;
    }
    return ans;
}
