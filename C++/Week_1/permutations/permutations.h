#pragma once

#include <numeric>
#include <set>
#include <vector>
#include <stdexcept>

void GeneratePermutationsRec(std::vector<std::vector<int>>& permutations, std::vector<int> cur, std::set<int> remain) {
    if (remain.empty()) {
        permutations.push_back(cur);
        return;
    }
    for (auto elem : remain) {
        auto cur_ = cur;
        cur_.push_back(elem);
        auto remain_ = remain;
        remain_.erase(elem);
        GeneratePermutationsRec(permutations, cur_, remain_);
    }
}

std::vector<std::vector<int>> GeneratePermutations(size_t len) {
    std::vector<std::vector<int>> permutations;
    std::vector<int> remain(len);
    std::iota(remain.begin(), remain.end(), 0);
    GeneratePermutationsRec(permutations, {}, std::set<int>(remain.begin(), remain.end()));
    return permutations;
}
