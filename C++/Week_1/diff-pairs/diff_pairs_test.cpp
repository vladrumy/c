#include <catch.hpp>

#include <diff_pairs.h>

TEST_CASE("Your test here") {
    REQUIRE(4 == CountPairs({2, 2, 3, 3}, 5));
    REQUIRE(6 == CountPairs({2, 2, 2, 2}, 4));
    REQUIRE(6 == CountPairs({1, 1, 2, 2, 2, 2}, 4));
    REQUIRE(2 == CountPairs({1, 2, 3, 4, 5}, 6));
    REQUIRE(2 == CountPairs({1, 2, 3, 4, 5, 6}, 5));
}
