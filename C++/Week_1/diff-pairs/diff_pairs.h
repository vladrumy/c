#pragma once

#include <iostream>
#include <vector>
#include <stdexcept>
#include <unordered_map>

inline int64_t CountPairs(const std::vector<int>& data, int x) {
    std::unordered_map<int, int> num_count;
    int64_t ans = 0;
    for (auto num : data) {
        ans += num_count[x - num];
        ++num_count[num];
    }
    return ans;
}
