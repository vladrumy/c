#pragma once

#include <string>
#include <unordered_map>
#include <stdexcept>

std::unordered_map<int, std::string> ReverseMap(std::unordered_map<std::string, int> map) {
    std::unordered_map<int, std::string> reverse_map;
    for (auto& [key, value] : map) {
        reverse_map[value] = key;
    }
    return reverse_map;
}
