#pragma once

#include <stack>
#include <string>
#include <stdexcept>
//#include "split/split.h"

std::vector<std::string> Split(const std::string& string, const std::string& delimiter = " ") {
    auto substr_begin = 0;
    std::vector<std::string> splitted;
    while (string.substr(substr_begin).find(delimiter) != std::string::npos) {
        splitted.push_back(string.substr(substr_begin, string.substr(substr_begin).find(delimiter)));
        substr_begin += string.substr(substr_begin).find(delimiter) + delimiter.size();
    }
    splitted.push_back(string.substr(substr_begin));
    return splitted;
}

int Solve(int first, int second, std::string oper) {
    if (oper == "+") {
        return first + second;
    } else if (oper == "-") {
        return first - second;
    } else if (oper == "*") {
        return first * second;
    }
    return 0;
}

int EvaluateExpression(const std::string& expression) {
    std::vector<std::string> operations = {"+", "-", "*"};
    auto parsed_exp = Split(expression);
    std::stack<int> res;
    for (auto elem : parsed_exp) {
        if (std::find(operations.begin(), operations.end(), elem) != operations.end()) {
            auto second = res.top();
            res.pop();
            auto first = res.top();
            res.pop();
            res.push(Solve(first, second, elem));
        } else {
            res.push(stoi(elem));
        }
    }
    auto result = res.top();
    return result;
}
