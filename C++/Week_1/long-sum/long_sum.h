#pragma once

#include <string>
#include <stdexcept>
#include <iostream>

inline std::string LongSum(const std::string& a, const std::string& b) {
    int i = 0;
    int remainder = 0;
    std::string ans = "";
    while (i < a.size() && i < b.size()) {
        auto cur = (a[a.size() - 1 - i] - '0') + (b[b.size() - 1 - i] - '0') + remainder;
        remainder = cur / 10;
        ans += std::to_string(cur % 10);
        ++i;
    };
    while (i < a.size()) {
        auto cur = (a[a.size() - 1 - i] - '0') + remainder;
        remainder = cur / 10;
        ans += std::to_string(cur % 10);
        ++i;
    };
    while (i < b.size()) {
        auto cur = (b[b.size() - 1 - i] - '0') + remainder;
        remainder = cur / 10;
        ans += std::to_string(cur % 10);
        ++i;
    };
    if (remainder) {
        ans += "1";
    }
    std::reverse(ans.begin(), ans.end());
    return ans;
}
