#include <catch.hpp>

#include <long_sum.h>

#include <algorithm>
#include <vector>

TEST_CASE("Your test") {
    REQUIRE("4" == LongSum("2", "2"));
    REQUIRE("294" == LongSum("150", "144"));
    REQUIRE("224" == LongSum("150", "74"));
    REQUIRE("1224" == LongSum("650", "574"));
}
