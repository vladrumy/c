#pragma once

#include <string>
#include <stdexcept>
#include <vector>
#include <iostream>

std::vector<std::string> Split(const std::string& string, const std::string& delimiter = " ") {
    auto substr_begin = 0;
    std::vector<std::string> splitted;
    while (string.substr(substr_begin).find(delimiter) != std::string::npos) {
        splitted.push_back(string.substr(substr_begin, string.substr(substr_begin).find(delimiter)));
        substr_begin += string.substr(substr_begin).find(delimiter) + delimiter.size();
    }
    splitted.push_back(string.substr(substr_begin));
    return splitted;
}
