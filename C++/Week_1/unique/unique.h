#pragma once

#include <vector>
#include <stdexcept>
#include <iostream>

std::vector<int> Unique(const std::vector<int>& data) {
    std::vector<int> ans;
    for (auto elem : data) {
        if (ans.empty() || ans.back() != elem) {
            ans.push_back(elem);
        }
    }
    return ans;
}
