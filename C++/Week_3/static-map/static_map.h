#pragma once

#include <vector>
#include <string>
#include <utility>

class StaticMap {
public:
    explicit StaticMap(const std::vector<std::pair<std::string, std::string>>& items) : items_(items) {
        std::sort(items_.begin(), items_.end());
    }

    bool Find(const std::string& key, std::string* value) const {
        auto elem = std::lower_bound(items_.begin(), items_.end(), std::make_pair(key, std::string("")));
        if (elem == items_.end() || elem->first != key) {
            return false;
        }
        *value = elem->second;
        return true;
    }

private:
    std::vector<std::pair<std::string, std::string>> items_;
};
