#include <string>
#include <cstring>

class StringView {
public:
    StringView(const std::string& str, size_t pos = 0, size_t len = std::string::npos)
        : data_(str.data() + pos), size_(std::min(len, str.size() - pos)) {}

    StringView(const char* str) : data_(str), size_(std::strlen(str)) {}

    StringView(const char* str, size_t len) : data_(str), size_(len) {}

    const char& operator[](size_t i) const {
        return data_[i];
    }

    size_t Size() const {
        return size_;
    }

private:
    const char* data_;
    size_t size_;
};
