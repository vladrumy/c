#pragma once

#include <vector>
#include <iostream>

class Matrix {
public:
    Matrix(unsigned long rows, unsigned long columns) : rows_(rows), columns_(columns) {
        matrix_.assign(rows_, std::vector<double>(columns_));
    }

    Matrix(unsigned long size) : rows_(size), columns_(size) {
        matrix_.assign(rows_, std::vector<double>(columns_));
    }

    Matrix(std::vector<std::vector<double>> matrix) : rows_(matrix.size()), matrix_(matrix) {
        if (rows_) {
            columns_ = matrix_[0].size();
        } else {
            columns_ = 0;
        }
    }

    unsigned long Rows() const {
        return rows_;
    }

    unsigned long Columns() const {
        return columns_;
    }

    double operator()(unsigned long row, unsigned long column) const {
        return matrix_[row][column];
    }

    double& operator()(unsigned long row, unsigned long column) {
        return matrix_[row][column];
    }

    Matrix& operator+=(const Matrix& rhs) {
        for (unsigned long i = 0; i < rhs.rows_; ++i) {
            for (unsigned long j = 0; j < rhs.columns_; ++j) {
                this->matrix_[i][j] += rhs.matrix_[i][j];
            }
        }
        return *this;
    }

    friend Matrix operator+(Matrix lhs, const Matrix& rhs) {
        return lhs += rhs;
    }

    Matrix& operator-=(const Matrix& rhs) {
        for (unsigned long i = 0; i < rhs.rows_; ++i) {
            for (unsigned long j = 0; j < rhs.columns_; ++j) {
                this->matrix_[i][j] -= rhs.matrix_[i][j];
            }
        }
        return *this;
    }

    friend Matrix operator-(Matrix lhs, const Matrix& rhs) {
        return lhs -= rhs;
    }

    friend Matrix operator*(const Matrix& lhs, const Matrix& rhs) {
        std::vector<std::vector<double>> prod(lhs.rows_, std::vector<double>(rhs.columns_));
        for (unsigned long i = 0; i < lhs.rows_; ++i) {
            for (unsigned long j = 0; j < lhs.columns_; ++j) {
                for (unsigned long k = 0; k < rhs.columns_; ++k) {
                    prod[i][k] += lhs.matrix_[i][j] * rhs.matrix_[j][k];
                }
            }
        }
        return Matrix(prod);
    }

    Matrix& operator*=(const Matrix& rhs) {
        return *this = *this * rhs;
    }

    friend Matrix Transpose(const Matrix& matrix);

    friend Matrix Identity(unsigned long n);

    void Cout(Matrix matrix) {
        for (auto row : matrix.matrix_) {
            for (auto elem : row) {
                std::cout << elem << " ";
            }
            std::cout << std::endl;
        }
    }

private:
    unsigned long rows_, columns_;
    std::vector<std::vector<double>> matrix_;
};

Matrix Transpose(const Matrix& matrix) {
    std::vector<std::vector<double>> transposed(matrix.columns_, std::vector<double>(matrix.rows_));
    for (unsigned long i = 0; i < matrix.rows_; ++i) {
        for (unsigned long j = 0; j < matrix.columns_; ++j) {
            transposed[j][i] = matrix.matrix_[i][j];
        }
    }
    return Matrix(transposed);
}

Matrix Identity(unsigned long n) {
    std::vector<std::vector<double>> identity(n, std::vector<double>(n));
    for (unsigned long i = 0; i < n; ++i) {
        identity[i][i] = 1.0;
    }
    return Matrix(identity);
}
