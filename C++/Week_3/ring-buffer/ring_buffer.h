#pragma once

#include <algorithm>
#include <vector>

class RingBuffer {
public:
    explicit RingBuffer(size_t capacity) : capacity_(capacity) {}

    size_t Size() const {
        return vec_.size();
    }

    bool Empty() const {
        return vec_.empty();
    }

    bool TryPush(int element) {
        if (vec_.size() == capacity_) {
            return false;
        }
        vec_.push_back(element);
        return true;
    }

    bool TryPop(int *element) {
        if (vec_.empty()) {
            return false;
        }
        *element = vec_.front();
        std::rotate(vec_.begin(), vec_.begin() + 1, vec_.end());
        vec_.pop_back();
        return true;
    }

private:
    size_t capacity_;
    std::vector<int> vec_;
};
