#pragma once

#include "dungeon.h"

#include <queue>
#include <set>
#include <unordered_map>
#include <vector>

#include <iostream>

Room* FindFinalRoom(Room* starting_room) {
//    std::cout << std::endl << "Start" << std::endl;
    std::queue<Room*> rooms;
    std::set<Door*> doors;
    std::vector<std::string> keys;
    std::unordered_map<Room*, int> visited;
    rooms.push(starting_room);
    while (!rooms.empty()) {
        //        std::cout << "rooms size : " << rooms.size() << std::endl;
        auto room = rooms.front();
//        std::cout << "room : " << room << " " << room->IsFinal() << std::endl;
        //        std::cout << "locked doors : " << doors.size() << std::endl;
        if (room->IsFinal()) {
            return room;
        }
        rooms.pop();
        visited[room] = 1;
        for (auto i = 0; i < room->NumKeys(); ++i) {
            auto key = room->GetKey(i);
//            std::cout << "found key : " << key << std::endl;
            keys.push_back(key);
            for (auto it = doors.begin(); it != doors.end();) {
                auto door = *it;
                if (door->TryOpen(key) && !visited[door->GoThrough()]) {
                    it = doors.erase(it);
//                    std::cout << "opened door " << door << " with key " << key << std::endl;
                    rooms.push(door->GoThrough());
//                    std::cout << "added room " << door->GoThrough() << std::endl;
                    //                    std::cout << rooms.front() << std::endl;
                    //                    std::cout << door->GoThrough()->IsFinal() << std::endl;
                } else {
                    ++it;
                }
            }
        }
        for (auto i = 0; i < room->NumDoors(); ++i) {
            auto door = room->GetDoor(i);
            if (door->IsOpen() && !visited[door->GoThrough()]) {
                rooms.push(door->GoThrough());
//                std::cout << "added room " << door->GoThrough() << std::endl;
                continue;
            }
            bool is_open = false;
            for (auto key : keys) {
                if (door->TryOpen(key) && !visited[door->GoThrough()]) {
                    rooms.push(door->GoThrough());
                    is_open = true;
                    continue;
                }
            }
            if (!is_open) {
                doors.insert(door);
//                std::cout << "added door " << door << std::endl;
            }
        }
    }
    return nullptr;
}
