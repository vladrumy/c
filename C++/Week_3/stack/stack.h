#pragma once

#include <cstdint>
#include <vector>

class Stack {
public:
    void Push(int x) {
        vec_.push_back(x);
    }

    bool Pop() {
        if (vec_.empty()) {
            return false;
        }
        vec_.pop_back();
        return true;
    }

    int Top() const {
        return vec_.back();
    }

    bool Empty() const {
        return vec_.empty();
    }

    size_t Size() const {
        return vec_.size();
    }

private:
    std::vector<int> vec_;
};
