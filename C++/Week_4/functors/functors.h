#pragma once

#include <vector>

template <class Functor>
class ReverseBinaryFunctor {
public:
    ReverseBinaryFunctor(Functor f) : f_(f) {}

    template <class T, class U>
    bool operator()(const T& arg1, const U& arg2) const {
        return f_(arg2, arg1);
    }

private:
    Functor f_;
};

template <class Functor>
class ReverseUnaryFunctor {
public:
    ReverseUnaryFunctor(Functor f) : f_(f) {}

    template <class T>
    bool operator()(const T& arg) const {
        return !f_(arg);
    }

private:
    Functor f_;
};

template <class Functor>
ReverseUnaryFunctor<Functor> MakeReverseUnaryFunctor(Functor functor) {
    return ReverseUnaryFunctor<Functor>(functor);
}

template <class Functor>
ReverseBinaryFunctor<Functor> MakeReverseBinaryFunctor(Functor functor) {
    return ReverseBinaryFunctor<Functor>(functor);
}

template <typename Iterator, typename Compare = std::less<>>
std::size_t ComparisonsCount(Iterator first, Iterator last, Compare comp = Compare()) {
    std::size_t count = 0;
    std::function<bool(typename std::iterator_traits<Iterator>::value_type, typename std::iterator_traits<Iterator>::value_type)> wrapped_comp =
        [&](typename std::iterator_traits<Iterator>::value_type a, typename std::iterator_traits<Iterator>::value_type b) {
            ++count;
            return comp(a, b);
        };
    std::sort(first, last, wrapped_comp);
    return count;
}
