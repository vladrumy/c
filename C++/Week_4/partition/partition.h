#pragma once

#include <stdexcept>

template <class Iterator, class Predicate>
Iterator Partition(Iterator first, Iterator last, Predicate pred) {
    auto first_unsatisfying = first;
    while (first != last) {
        if (pred(*first)) {
            std::swap(*first, *first_unsatisfying);
            first_unsatisfying = std::next(first_unsatisfying);
        }
        first = std::next(first);
    }
    return first_unsatisfying;
}
