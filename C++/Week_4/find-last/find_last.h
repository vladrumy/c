#pragma once

#include <stdexcept>

template <class Iterator, class T>
Iterator FindLast(Iterator first, Iterator last, const T& val) {
    auto ans = last;
    for (auto it = first; it != last; it = std::next(it)) {
        if (*it == val) {
            ans = it;
        }
    }
    return ans;
}
