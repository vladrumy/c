#pragma once

struct Sum {
    template <class T>
    T operator() (T lhs, T rhs) {
        return lhs + rhs;
    }
};

struct Prod {
    template <class T>
    T operator() (T lhs, T rhs) {
        return lhs * rhs;
    }
};

struct Concat {
    template <class T>
    T operator() (T& lhs, T& rhs) {
        auto ans = lhs;
        ans.insert(ans.end(), rhs.begin(), rhs.end());
        return ans;
    }
};

template <class Iterator, class T, class BinaryOp>
T Fold(Iterator first, Iterator last, T init, BinaryOp func) {
    auto res = init;
    while(first != last) {
        res = func(res, *first);
        first = std::next(first);
    }
    return res;
}

class Length {
public:
    Length() {}

    Length(int* length) : length_(length) {}

    template <class T>
    T operator()(T& lhs, T& rhs) const {
        ++(*length_);
        return rhs;
    }

private:
    int* length_;
};
