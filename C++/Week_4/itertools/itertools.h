#pragma once

template <class Iterator>
class IteratorRange {
public:
    IteratorRange(Iterator begin, Iterator end) : begin_(begin), end_(end) {
    }

    Iterator begin() const {  // NOLINT
        return begin_;
    }

    Iterator end() const {  // NOLINT
        return end_;
    }

private:
    const Iterator begin_, end_;
};

class RangeIterator {
public:
    RangeIterator(int64_t pos, int64_t step) : pos_(pos), step_(step) {}

    bool operator!=(RangeIterator rhs) {
        return pos_ < rhs.pos_;
    }

    auto operator++() {
        pos_ += step_;
        return *this;
    }

    auto operator*() {
        return pos_;
    }

    auto operator-(RangeIterator rhs) {
        return pos_ - rhs.pos_;
    }

    auto operator+=(int rhs) {
        pos_ += rhs;
        return *this;
    }

private:
    int64_t pos_;
    int64_t step_;
};

auto operator+(RangeIterator lhs, int rhs) {
    lhs += rhs;
    return lhs;
}

IteratorRange<RangeIterator> Range(int64_t from, int64_t to, int step) {
    return IteratorRange(RangeIterator(from, step), RangeIterator(to, step));
}

IteratorRange<RangeIterator> Range(int64_t from, int64_t to) {
    int64_t step = 1;
    return IteratorRange(RangeIterator(from, step), RangeIterator(to, step));
}

IteratorRange<RangeIterator> Range(int64_t to) {
    int64_t step = 1;
    int64_t from = 0;
    return IteratorRange(RangeIterator(from, step), RangeIterator(to, step));
}

template <class Iter1, class Iter2>
class ZipIterator {
public:
    ZipIterator(Iter1 iter1, Iter2 iter2) : iter_first_(iter1), iter_second_(iter2) {}

    bool operator!=(ZipIterator rhs) {
        return iter_first_ != rhs.iter_first_ || iter_second_ != rhs.iter_second_;
    }

    auto operator++() {
        ++iter_first_;
        ++iter_second_;
        return *this;
    }

    auto operator*() {
        return std::make_pair(*iter_first_, *iter_second_);
    }

private:
    Iter1 iter_first_;
    Iter2 iter_second_;
};

template <class Type1>
auto Zip(const Type1& seq1, IteratorRange<RangeIterator> seq2) {
    auto size = std::min(static_cast<int64_t>(seq1.size()), seq2.end() - seq2.begin());
    return IteratorRange(ZipIterator(seq1.begin(), seq2.begin()),
                         ZipIterator(seq1.begin() + size, seq2.begin() + size));
}
template <class Type1>
auto Zip(IteratorRange<RangeIterator> seq1, const Type1& seq2) {
    auto size = std::min(static_cast<int64_t>(seq2.size()), seq1.end() - seq1.begin());
    return IteratorRange(ZipIterator(seq1.begin(), seq2.begin()),
                         ZipIterator(seq1.begin() + size, seq2.begin() + size));
}

template <class Type1, class Type2>
auto Zip(const Type1& seq1, const Type2& seq2) {
    return IteratorRange(ZipIterator(seq1.begin(), seq2.begin()),
                         ZipIterator(seq1.end(), seq2.end()));
}

template <class Type>
class SmallGroupIterator {
public:
    SmallGroupIterator(Type value, int64_t pos) : value_(value), pos_(pos) {
    }

    bool operator==(SmallGroupIterator rhs) {
        return (value_ == rhs.value_) && (pos_ == rhs.pos_);
    }

    bool operator!=(SmallGroupIterator rhs) {
        return !((*this) == rhs);
    }

    auto operator++() {
        ++pos_;
        return *this;
    }

    auto operator*() {
        return value_;
    }

private:
    Type value_;
    int64_t pos_;
};

template <class Iter>
class GroupIterator {
public:
    GroupIterator(std::vector<Iter> points, std::vector<size_t> points_pos, size_t pos)
        : points_(points), points_pos_(points_pos), pos_(pos) {
    }

    bool operator==(GroupIterator rhs) {
        return pos_ == rhs.pos_;
    }

    bool operator!=(GroupIterator rhs) {
        return !((*this) == rhs);
    }

    auto operator++() {
        pos_++;
        return *this;
    }

    auto operator*() {
        return IteratorRange(
            SmallGroupIterator(*(points_[pos_]), 0),
            SmallGroupIterator(*(points_[pos_]), points_pos_[pos_ + 1] - points_pos_[pos_]));
    }

private:
    std::vector<Iter> points_;
    std::vector<size_t> points_pos_;
    size_t pos_;
};

template <class Type>
auto Group(const Type& seq) {
    if (seq.empty()) {
        std::vector<typename Type::const_iterator> hot_points;
        return IteratorRange(GroupIterator(hot_points, {}, 0), GroupIterator(hot_points, {}, 0));
    }
    std::vector<typename Type::const_iterator> hot_points;
    hot_points.push_back(seq.begin());
    std::vector<size_t> hot_points_pos;
    hot_points_pos.push_back(0);
    auto old_elem = *(seq.begin());
    int i = 0;
    for (auto iter = seq.begin(); iter != seq.end(); ++iter) {
        if (i) {
            if (!(old_elem == *iter)) {
                hot_points.push_back(iter);
                hot_points_pos.push_back(i);
            }
        }
        old_elem = *iter;
        i++;
    }
    hot_points_pos.push_back(seq.size());
    return IteratorRange(GroupIterator(hot_points, hot_points_pos, 0),
                         GroupIterator(hot_points, hot_points_pos, hot_points.size()));
}
