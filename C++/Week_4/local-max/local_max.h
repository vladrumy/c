#pragma once

#include <stdexcept>

template <class Iterator>
Iterator LocalMax(Iterator first, Iterator last) {
    if (first == last) {
        return last;
    }
    first = std::next(first, 1);
    if (first == last) {
        return last;
    }
    while (std::next(first, 1) != last) {
        if (*first > *std::next(first, -1) && *first > *std::next(first, 1)) {
            return first;
        }
        first = std::next(first, 1);
    }
    return last;
}
