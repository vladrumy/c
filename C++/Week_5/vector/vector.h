#pragma once

class Vector {
private:
    int* data_;
    size_t size_;
    size_t capacity_;

public:
    explicit Vector() :
          data_(new int[1]),
          size_(0),
          capacity_(1)
    {}

    explicit Vector(size_t size) :
          data_(new int[size]),
          size_(size),
          capacity_(size)
    {
        for (int i = 0; i < size; ++i) {
            data_[i] = 0;
        }
    }

    Vector(std::initializer_list<int> list) :
          data_(new int[list.size()]),
          size_(list.size()),
          capacity_(list.size())
    {
        size_t i = 0;
        for (int cur : list) {
            data_[i++] = cur;
        }
    }

    Vector(const Vector& rhs) :
          data_(new int[rhs.capacity_]),
          size_(rhs.size_),
          capacity_(rhs.capacity_)
    {
        for (size_t i = 0; i < rhs.size_; ++i) {
            data_[i] = rhs.data_[i];
        }
    }

    Vector(Vector&& rhs) :
          data_(rhs.data_),
          size_(rhs.size_),
          capacity_(rhs.capacity_)
    {
        rhs.data_ = nullptr;
    }

    Vector& operator=(Vector& rhs) {
        Swap(rhs);
        return *this;
    }

    Vector& operator=(Vector&& rhs) {
        delete[] data_;
        data_ = rhs.data_;
        size_ = rhs.size_;
        capacity_ = rhs.capacity_;
        rhs.data_ = nullptr;
        return *this;
    }

    ~Vector() {
        delete[] data_;
    }

    void Swap(Vector& rhs) {
        std::swap(data_, rhs.data_);
        std::swap(size_, rhs.size_);
        std::swap(capacity_, rhs.capacity_);
    }

    int& operator[](size_t index) {
        return data_[index];
    }

    const int& operator[](size_t index) const {
        return data_[index];
    }

    size_t Size() const {
        return size_;
    }

    size_t Capacity() const {
        return capacity_;
    }

    void PushBack(int elem) {
        if (size_ >= capacity_) {
            Reserve(capacity_ * 2);
        }
        data_[size_++] = elem;
    }

    void PopBack() {
        if (size_) {
            data_[--size_] = 0;
        }
    }

    void Clear() {
        while (size_) {
            data_[--size_] = 0;
        }
    }

    void Reserve(size_t capacity) {
        if (capacity > capacity_) {
            auto* new_data = new int[capacity];
            for (int i = 0; i < size_; ++i) {
                new_data[i] = data_[i];
            }
            capacity_ = capacity;
            delete[] data_;
            data_ = new_data;
        }
    }

    class Iterator : public std::iterator<std::random_access_iterator_tag, int, int> {
    private:
        int* current_;

    public:
        Iterator() {}

        Iterator(int* current) :
              current_(current)
        {}

        Iterator& operator++() {
            ++current_;
            return *this;
        }

        Iterator operator++(int) {
            return current_++;
        }

        Iterator& operator--() {
            --current_;
            return *this;
        }

        Iterator operator--(int) {
            return current_--;
        }

        bool operator==(const Iterator& rhs) const {
            return current_ == rhs.current_;
        }

        bool operator!=(const Iterator& rhs) const {
            return !(*this == rhs);
        }

        int& operator*() {
            return *current_;
        }

        int* operator->() {
            return current_;
        }

        Iterator& operator+=(int n) {
            current_ += n;
            return *this;
        }

        Iterator operator+(int n) {
            Iterator temp = *this;
            return temp += n;
        }

        Iterator& operator-=(int n) {
            return *this += -n;
        }

        Iterator operator-(int n) {
            Iterator temp = *this;
            return temp -= n;
        }

        int operator-(const Iterator& rhs) {
            Iterator tmp = *this;
            int res = 0;
            while (tmp-- != rhs) {
                ++res;
            }
            return res;
        }
    };
    Iterator begin() {  // NOLINT
        return Iterator(data_);
    }

    Iterator end() {  // NOLINT
        return Iterator(data_ + size_);
    }

    Iterator Begin() {
        return Iterator(data_);
    }

    Iterator End() {
        return Iterator(data_ + size_);
    }
};
