#pragma once

#include <list>
#include <string>
#include <unordered_map>

class LruCache {
private:
    std::unordered_map<std::string, std::list<std::pair<std::string, std::string>>::iterator> indices_;
    std::list<std::pair<std::string, std::string>> values_;
    size_t max_size_;

public:
    LruCache(size_t max_size) :
          max_size_(max_size)
    {}

    void Set(const std::string& key, const std::string& value) {
        if (values_.size() == max_size_) {
            indices_.erase(values_.back().first);
            values_.pop_back();
        }
        values_.push_front(std::make_pair(key, value));
        indices_[key] = values_.begin();
    }

    bool Get(const std::string& key, std::string* value) {
        const auto index = indices_.find(key);
        if (index == indices_.end()) {
            return false;
        }
        *value = index->second->second;
        values_.erase(index->second);
        values_.push_front(std::make_pair(key, *value));
        indices_[key] = values_.begin();
        return true;
    }
};
