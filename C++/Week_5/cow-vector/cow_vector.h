#pragma once

struct State {
    int ref_count;
    std::vector<std::string> data;
};

class COWVector {
private:
    State* state_;

public:
    COWVector() :
          state_(new State)
    {
        state_->ref_count = 1;
        state_->data = {};
    }

    ~COWVector() {
        if (state_->ref_count == 1) {
            delete state_;
        } else {
            --state_->ref_count;
        }
    }

    COWVector(const COWVector& other) {
        state_ = other.state_;
        ++state_->ref_count;
    }

    COWVector& operator=(const COWVector& other) {
        state_ = other.state_;
        ++state_->ref_count;
        return *this;
    }

    size_t Size() const {
        return state_->data.size();
    }

    void Resize(size_t size) {
        CopyState();
        state_->data.resize(size);
    }

    const std::string& Get(size_t at) {
        return state_->data[at];
    }

    const std::string& Back() {
        return state_->data.back();
    }

    void PushBack(const std::string& value) {
        CopyState();
        state_->data.push_back(value);
    }

    void Set(size_t at, const std::string& value) {
        CopyState();
        state_->data[at] = value;
    }

    void CopyState() {
        if (state_->ref_count > 1) {
            auto* new_state = new State;
            new_state->ref_count = 1;
            new_state->data = state_->data;
            state_ = new_state;
        }
    }
};
