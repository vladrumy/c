#pragma once
#include <stdexcept>
#include <map>
#include <vector>
#include <functional>

class Substr {
private:
    std::string str_;

public:
    Substr(const std::string& str) :
        str_(str)
    {}

    bool operator()(const std::string& cur_str) {
        return cur_str.find(str_) != std::string::npos;
    }
};

class FullMatch {
private:
    std::string str_;

public:
    FullMatch(const std::string& str) :
        str_(str)
    {}

    bool operator()(const std::string& cur_str) {
        return cur_str == str_;
    }
};

class AbstractTest {
public:
    virtual ~AbstractTest() = default;
    virtual void SetUp() = 0;
    virtual void TearDown() = 0;
    virtual void Run() = 0;
};

class ITestProducer {
public:
    virtual ~ITestProducer() = default;
    virtual std::unique_ptr<AbstractTest> Produce() = 0;
};

template <class Test>
class TestProducer : public ITestProducer {
public:
    std::unique_ptr<AbstractTest> Produce() override {
        return std::make_unique<Test>();
    }
};

class TestRegistry {
private:
    std::map<std::string, std::unique_ptr<ITestProducer>> producers_;

public:
    TestRegistry() = default;

    template <class TestClass>
    void RegisterClass(const std::string& class_name) {
        producers_.emplace(class_name, std::make_unique<TestProducer<TestClass>>());
    }

    std::unique_ptr<AbstractTest> CreateTest(const std::string& class_name) {
        auto it = producers_.find(class_name);
        if (it == producers_.end()) {
            throw std::out_of_range("out_of_range");
        }
        return it->second->Produce();
    }

    void RunTest(const std::string& test_name) {
        auto cur_test = CreateTest(test_name);
        cur_test->SetUp();
        try {
            cur_test->Run();
        } catch (...) {
            cur_test->TearDown();
            throw;
        }
        cur_test->TearDown();
    }

    template <class Predicate>
    std::vector<std::string> ShowTests(Predicate callback) const {
        std::vector<std::string> res;
        for (const auto& name : producers_) {
            if (callback(name.first)) {
                res.emplace_back(name.first);
            }
        }
        return res;
    }

    std::vector<std::string> ShowAllTests() const {
        std::vector<std::string> res;
        for (const auto& name : producers_) {
            res.emplace_back(name.first);
        }
        return res;
    }

    template <class Predicate>
    void RunTests(Predicate callback) {
        for (const auto& name : producers_) {
            if (callback(name.first)) {
                RunTest(name.first);
            }
        }
    }

    void Clear() {
        producers_.clear();
    }

    static TestRegistry& Instance() {
        static TestRegistry instance;
        return instance;
    }
};
