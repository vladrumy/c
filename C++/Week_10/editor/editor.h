#pragma once

class ICommand {
public:
    virtual void Do(bool) = 0;
    virtual void Undo() = 0;
    virtual ~ICommand() = default;
};

class TypeCommand;
class ShiftLeftCommand;
class ShiftRightCommand;
class BackspaceCommand;

class Editor {
private:
    int cursor_ = 0;
    std::string text_ = {};
    std::vector<std::unique_ptr<ICommand>> done_ = {};
    std::vector<std::unique_ptr<ICommand>> undone_ = {};

public:
    const std::string& GetText() const {
        return text_;
    }

    void Type(char c) {
        ExecuteCommand<TypeCommand>(c);
    }

    void ShiftLeft() {
        if (cursor_ == 0) {
            return;
        }
        ExecuteCommand<ShiftLeftCommand>();
    }

    void ShiftRight() {
        if (cursor_ == text_.size()) {
            return;
        }
        ExecuteCommand<ShiftRightCommand>();
    }

    void Backspace() {
        if (cursor_ == 0) {
            return;
        }
        ExecuteCommand<BackspaceCommand>();
    }

    void Undo() {
        if (done_.empty()) {
            return;
        }
        done_.back()->Undo();
        undone_.emplace_back(std::move(done_.back()));
        done_.pop_back();
    }

    void Redo() {
        if (undone_.empty()) {
            return;
        }
        undone_.back()->Do(true);
        done_.emplace_back(std::move(undone_.back()));
        undone_.pop_back();
    }

private:
    void AppendChar(char symbol) {
        text_.insert(cursor_++, 1, symbol);
    }

    void EraseChar() {
        text_.erase(--cursor_, 1);
    }

    template <typename CommandType, typename... Args>
    void ExecuteCommand(Args&&... args) {
        done_.emplace_back(std::make_unique<CommandType>(*this, std::forward<Args>(args)...));
        done_.back()->Do(false);
    }

    friend class TypeCommand;
    friend class ShiftLeftCommand;
    friend class ShiftRightCommand;
    friend class BackspaceCommand;
};

class TypeCommand : public ICommand {
private:
    Editor& file_;
    char symbol_;

public:
    TypeCommand(Editor& file, char symbol) :
        file_(file)
        , symbol_(symbol)
    {}

    void Do(bool is_redo) override {
        if (!is_redo) {
            file_.undone_.clear();
        }
        file_.AppendChar(symbol_);
    }

    void Undo() override {
        file_.EraseChar();
    }
};

class ShiftLeftCommand : public ICommand {
private:
    Editor& file_;

public:
    ShiftLeftCommand(Editor& file) : file_(file) {
    }

    void Do(bool is_redo) override {
        if (!is_redo) {
            file_.undone_.clear();
        }
        file_.cursor_--;
    }

    void Undo() override {
        file_.cursor_++;
    }
};

class ShiftRightCommand : public ICommand {
private:
    Editor& file_;

public:
    ShiftRightCommand(Editor& file) : file_(file) {
    }

    void Do(bool is_redo) override {
        if (!is_redo) {
            file_.undone_.clear();
        }
        file_.cursor_++;
    }

    void Undo() override {
        file_.cursor_--;
    }
};

class BackspaceCommand : public ICommand {
private:
    Editor& file_;
    char symbol_;

public:
    BackspaceCommand(Editor& file)
        : file_(file)
    {}

    void Do(bool is_redo) override {
        if (!is_redo) {
            file_.undone_.clear();
        }
        symbol_ = file_.text_[file_.cursor_ - 1];
        file_.EraseChar();
    }

    void Undo() override {
        file_.AppendChar(symbol_);
    }
};
