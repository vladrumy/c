#pragma once

template <class T>
class List;

template <class T>
class ListNode {
private:
    std::optional<T> value_;
    ListNode* next_;
    ListNode* prev_;

public:
    ListNode() :
        value_({})
    {}

    ListNode(const T& value) :
        value_(value)
    {}

    ListNode(T&& value) :
        value_(std::move(value))
    {}

    void Swap(ListNode& other) {
        std::swap(value_, other.value_);
        std::swap(prev_, other.prev_);
        std::swap(next_, other.next_);
    }

    friend class List<T>;
};


template <class T>
class List {
private:
    ListNode<T>* sentinel_;
    size_t size_;

public:
    class Iterator {
    private:
        ListNode<T>* current_;

    public:
        Iterator(ListNode<T>* ptr) :
            current_(ptr)
        {}

        Iterator& operator++() {
            current_ = current_->next_;
            return *this;
        }

        Iterator operator++(int) {
            current_ = current_->next_;
            return current_->prev_;
        }

        Iterator& operator--() {
            current_ = current_->prev_;
            return *this;
        }
        
        Iterator operator--(int) {
            current_ = current_->prev_;
            return current_->next_;
        }

        T& operator*() const {
            return *current_->value_;
        }
        
        T* operator->() const {
            return this;
        }

        bool operator==(const Iterator& other) const {
            return current_ == other.GetCurrent();
        }
        
        bool operator!=(const Iterator& other) const {
            return current_ != other.GetCurrent();
        }

        ListNode<T>* GetCurrent() const {
            return current_;
        }
    };

    void Unlink(ListNode<T>* node) {
        node->next_->prev_ = node->prev_;
        node->prev_->next_ = node->next_;
        delete node;
    }

    void LinkAfter(ListNode<T>* target, ListNode<T>* after) {
        after->prev_ = target;
        after->next_ = target->next_;
        target->next_->prev_ = after;
        target->next_ = after;
    }

    List() :
        size_(0)
    {
        sentinel_ = new ListNode<T>;
        sentinel_->prev_ = sentinel_;
        sentinel_->next_ = sentinel_;
    }

    List(const List& other) :
        size_(other.size_)
    {
        sentinel_ = new ListNode<T>;
        sentinel_->prev_ = sentinel_;
        sentinel_->next_ = sentinel_;

        auto previous = sentinel_;
        auto current = other.sentinel_->next_;
        while (current != other.sentinel_) {
            auto new_node = new ListNode<T>(*current->value_);
            LinkAfter(previous, new_node);
            previous = new_node;
            current = current->next_;
        }
        sentinel_->prev_ = current->prev_;
        previous->next_ = sentinel_;
    }

    List(List&& other) :
        sentinel_(other.sentinel_)
        , size_(other.size_)
    {
        other.sentinel_ = nullptr;
        other.size_ = 0;
    }

    ~List() {
        if (sentinel_ == nullptr) {
            return;
        }
        auto current = sentinel_->next_;
        while (current != sentinel_) {
            auto next = current->next_;
            delete current;
            current = next;
        }
        delete sentinel_;
    }

    List& operator=(const List& other) {
        sentinel_ = new ListNode<T>;
        sentinel_->prev_ = sentinel_;
        sentinel_->next_ = sentinel_;
        auto current = other.sentinel_->next_;
        auto previous = sentinel_;
        while (current != other.sentinel_) {
            auto new_node = new ListNode<T>(*current->value_);
            LinkAfter(previous, new_node);
            current = current->next_;
            previous = previous->next_;
        }
        sentinel_->prev_ = previous;
        previous->next_ = sentinel_;
        size_ = other.size_;
        return *this;
    }

    List& operator=(List&& other) {
        sentinel_ = other.sentinel_;
        size_ = other.size_;
        other.sentinel_ = nullptr;
        other.size_ = 0;
        return *this;
    }

// impossible to do operator= via swap because swap makes a copy
// and not increasing shared_ptr counter
//
//    List& operator=(List other) {
//        Swap(other);
//        return *this;
//    }

    void Swap(List& other) {
        sentinel_->Swap(*other.sentinel_);
        std::swap(size_, other.size_);
    }

    bool IsEmpty() const {
        return size_ == 0;
    }

    auto Size() const {
        return size_;
    }

    void PushBack(const T& elem) {
        auto new_node = new ListNode<T>(elem);
        LinkAfter(sentinel_->prev_, new_node);
        ++size_;
    }

    void PushBack(T&& elem) {
        auto new_node = new ListNode<T>(std::move(elem));
        LinkAfter(sentinel_->prev_, new_node);
        ++size_;
    }

    void PushFront(const T& elem) {
        auto new_node = new ListNode<T>(elem);
        LinkAfter(sentinel_, new_node);
        ++size_;
    }

    void PushFront(T&& elem) {
        auto new_node = new ListNode<T>(std::move(elem));
        LinkAfter(sentinel_, new_node);
        ++size_;
    }

    T& Front() {
        return *(sentinel_->next_)->value_;
    }

    const T& Front() const {
        return *(sentinel_->next_)->value_;
    }

    T& Back() {
        return *(sentinel_->prev_)->value_;
    }

    const T& Back() const {
        return *(sentinel_->prev_)->value_;
    }

    void PopBack() {
        Unlink(sentinel_->prev_);
        --size_;
    }

    void PopFront() {
        Unlink(sentinel_->next_);
        --size_;
    }

    Iterator Begin() {
        return Iterator(sentinel_->next_);
    }

    Iterator End() {
        return Iterator(sentinel_);
    }
};

template <typename T>
typename List<T>::Iterator begin(List<T>& list) {
    return list.Begin();
}

template <typename T>
typename List<T>::Iterator end(List<T>& list) {
    return list.End();
}
