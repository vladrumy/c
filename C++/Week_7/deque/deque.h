//#include <initializer_list>
//#include <algorithm>
//
//class Deque {
//public:
//    Deque() : data_(BlockSize) {
//        start_ = end_ = BlockSize / 2;
//    }
//
//    explicit Deque(size_t size) : data_(size + BlockSize) {
//        start_ = end_ = BlockSize / 2;
//        std::fill(begin(), end(), 0);
//    }
//
//    Deque(std::initializer_list<int> list) : data_(list.size() + BlockSize) {
//        start_ = end_ = BlockSize / 2;
//        std::copy(list.begin(), list.end(), begin());
//    }
//
//    Deque(const Deque& rhs) : data_(rhs.data_.size()) {
//        start_ = rhs.start_;
//        end_ = rhs.end_;
//        std::copy(rhs.begin(), rhs.end(), begin());
//    }
//
//    Deque(Deque&& rhs) noexcept : data_(std::move(rhs.data_)) {
//        start_ = rhs.start_;
//        end_ = rhs.end_;
//        rhs.start_ = rhs.end_ = BlockSize / 2;
//    }
//
//    Deque& operator=(Deque rhs) {
//        Swap(rhs);
//        return *this;
//    }
//
//    void Swap(Deque& rhs) noexcept {
//        data_.swap(rhs.data_);
//        std::swap(start_, rhs.start_);
//        std::swap(end_, rhs.end_);
//    }
//
//    void PushBack(int value) {
//        if (end_ == data_.size()) {
//            Resize();
//        }
//        data_[end_++] = value;
//    }
//
//    void PopBack() {
//        if (end_ == start_) {
//            return; // Deque is empty
//        }
//        --end_;
//    }
//
//    void PushFront(int value) {
//        if (start_ == 0) {
//            Resize();
//        }
//        data_[--start_] = value;
//    }
//
//    void PopFront() {
//        if (end_ == start_) {
//            return; // Deque is empty
//        }
//        ++start_;
//    }
//
//    int& operator[](size_t ind) {
//        return data_[start_ + ind];
//    }
//
//    int operator[](size_t ind) const {
//        return data_[start_ + ind];
//    }
//
//    size_t Size() const {
//        return end_ - start_;
//    }
//
//    void Clear() {
//        start_ = end_ = BlockSize / 2;
//    }
//
//private:
//    static constexpr size_t BlockSize = 512;
//    std::vector<int> data_;
//    size_t start_;
//    size_t end_;
//
//    int* begin() {
//        return data_.data() + start_;
//    }
//
//    const int* begin() const {
//        return data_.data() + start_;
//    }
//
//    void Resize() {
//        size_t newSize = data_.size() + BlockSize;
//        std::vector<int> newData(newSize);
//        size_t newStart = newSize / 2 - Size() / 2;
//        size_t newEnd = newStart + Size();
//        std::copy(begin(), end(), newData.begin() + newStart);
//        data_ = std::move(newData);
//        start_ = newStart;
//        end_ = newEnd;
//    }
//};




#pragma once

#include <cstddef>
#include <initializer_list>
#include <algorithm>

class Deque {
public:
    Deque() : data_(new int[INITIAL_CAPACITY]), size_(0), capacity_(INITIAL_CAPACITY), front_(0) {
    }

    explicit Deque(size_t size) : data_(new int[size]), size_(size), capacity_(size), front_(0) {
        std::fill(data_, data_ + size_, 0);
    }

    Deque(std::initializer_list<int> list) : data_(new int[list.size()]), size_(list.size()), capacity_(list.size()), front_(0) {
        std::copy(list.begin(), list.end(), data_);
    }

    Deque(const Deque& rhs) : data_(new int[rhs.capacity_]), size_(rhs.size_), capacity_(rhs.capacity_), front_(rhs.front_) {
        std::copy(rhs.data_, rhs.data_ + rhs.size_, data_);
    }

    Deque(Deque&& rhs) noexcept : data_(rhs.data_), size_(rhs.size_), capacity_(rhs.capacity_), front_(rhs.front_) {
        rhs.data_ = nullptr;
        rhs.size_ = 0;
        rhs.capacity_ = 0;
    }

    Deque& operator=(Deque rhs) {
        Swap(rhs);
        return *this;
    }

    ~Deque() {
        delete[] data_;
    }

    void Swap(Deque& rhs) noexcept {
        std::swap(data_, rhs.data_);
        std::swap(size_, rhs.size_);
        std::swap(capacity_, rhs.capacity_);
        std::swap(front_, rhs.front_);
    }

    void PushBack(int value) {
        if (size_ == capacity_)
            ExpandCapacity();

        auto back = (front_ + size_++) % capacity_;
        data_[back] = value;
    }

    void PopBack() {
        if (size_ == 0)
            return;

        --size_;
    }

    void PushFront(int value) {
        if (size_ == capacity_)
            ExpandCapacity();

        front_ = (front_ - 1) % capacity_;
        data_[front_] = value;
        size_++;
    }

    void PopFront() {
        if (size_ == 0)
            return;

        front_ = (front_ + 1) % capacity_;
        size_--;
    }

    int& operator[](size_t ind) {
        size_t index = (front_ + ind) % capacity_;
        return data_[index];
    }

    int operator[](size_t ind) const {
        size_t index = (front_ + ind) % capacity_;
        return data_[index];
    }

    size_t Size() const {
        return size_;
    }

    size_t Capacity() const {
        return capacity_;
    }

    void Clear() {
        size_ = 0;
        front_ = 0;
    }

private:
    static constexpr size_t INITIAL_CAPACITY = 4;

    int* data_;
    size_t size_;
    size_t capacity_;
    size_t front_;

    void ExpandCapacity() {
        size_t newCapacity = capacity_ * 2;
        int* newData = new int[newCapacity];

        for (size_t i = 0; i < size_; i++) {
            newData[i] = data_[(front_ + i) % capacity_];
        }

        delete[] data_;
        data_ = newData;
        capacity_ = newCapacity;
        front_ = 0;
    }
};
