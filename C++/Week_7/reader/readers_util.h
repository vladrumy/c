#pragma once

#include <memory>
#include <vector>

#include <reader.h>

class LimitReader : public Reader {
private:
    std::unique_ptr<Reader> reader_;
    size_t limit_;

public:
    LimitReader(std::unique_ptr<Reader> reader, size_t limit)
        : reader_(std::move(reader))
        , limit_(limit)
    {}

    virtual size_t Read(char* buf, size_t len) override {
        auto bytes_to_read = std::min(len, limit_);
        reader_->Read(buf, bytes_to_read);
        limit_ -= bytes_to_read;
        return bytes_to_read;
    }
};

class TeeReader : public Reader {
private:
    std::vector<std::unique_ptr<Reader>> readers_;

public:
    TeeReader(std::vector<std::unique_ptr<Reader>> readers) :
        readers_(std::move(readers))
    {}

    virtual size_t Read(char* buf, size_t len) override {
        size_t pos = 0;
        for (const auto& reader : readers_) {
            size_t cur_len;
            while (cur_len = reader->Read(buf + pos, len)) {
                pos += cur_len;
                len -= cur_len;
            }
        }
        return pos;
    }
};

class HexDecodingReader : public Reader {
private:
    std::unique_ptr<Reader> reader_;

//    char HexToInt(const char* hexStr) {
////        std::string hexNum(hexStr);
//        char result = 0;
//        for (; *hexStr != '\0'; ++hexStr) {
//            result *= 16;
//            if (*hexStr >= '0' && *hexStr <= '9') {
//                result += *hexStr - '0';
//            } else if (*hexStr >= 'a' && *hexStr <= 'f') {
//                result += 10 + *hexStr - 'a';
//            }
//        }
//
//        return result;
//    }

    char HexToDec(const char* hex) {
        char res = 0;
        for (auto i = 0; i < 2; ++i) {
            res *= 16;
            if (hex[i] >= 'a') {
                res += (hex[i] - 'a' + 10);
            } else {
                res += (hex[i] - '0');
            }
        }
        return res;
    }

public:
    HexDecodingReader(std::unique_ptr<Reader> reader) :
        reader_(std::move(reader))
    {}

    virtual size_t Read(char* buf, size_t len) override {
        size_t pos = 0;
        for (int i = 0; i < len * 2; i += 2) {
            char cur[2];
            if (!reader_->Read(cur, 2)) {
                return pos;
            }
            buf[pos++] = HexToDec(cur);
        }
        return len;
    }
};
