#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <memory>

class Tokenizer {
public:
    Tokenizer(std::istream* in) :
        in_(in)
    {
        Consume();
    }

    enum TokenType { kUnknown, kNumber, kSymbol, kEnd };

    void Consume() {
        char ch;
        *in_ >> ch;
        if (in_->eof()) {
            type_ = TokenType::kEnd;
            return;
        } else if (isdigit(ch)) {
            in_->putback(ch);
            *in_ >> number_;
            type_ = TokenType::kNumber;
        } else {
            symbol_ = ch;
            type_ = TokenType::kSymbol;
        }
    }

    TokenType GetType() {
        return type_;
    }

    int64_t GetNumber() {
        return number_;
    }

    char GetSymbol() {
        return symbol_;
    }

private:
    std::istream* in_;
    TokenType type_ = TokenType::kUnknown;
    int64_t number_;
    char symbol_;
};

class Expression {
public:
    virtual ~Expression() {}

    virtual int64_t Evaluate() = 0;
};

class ConstantExpression : public Expression {
private:
    int64_t value_;

public:
    explicit ConstantExpression(int64_t value) :
        value_(value)
    {}

    int64_t Evaluate() override {
        return value_;
    }
};

class BinaryExpression : public Expression {
public:
    BinaryExpression(std::unique_ptr<Expression> left, std::unique_ptr<Expression> right) :
        left_(std::move(left))
        , right_(std::move(right))
    {}

protected:
    std::unique_ptr<Expression> left_;
    std::unique_ptr<Expression> right_;
};

class AdditionExpression : public BinaryExpression {
public:
    using BinaryExpression::BinaryExpression;

    int64_t Evaluate() override {
        return left_->Evaluate() + right_->Evaluate();
    }
};

class SubtractionExpression : public BinaryExpression {
public:
    using BinaryExpression::BinaryExpression;

    int64_t Evaluate() override {
        return left_->Evaluate() - right_->Evaluate();
    }
};

class MultiplicationExpression : public BinaryExpression {
public:
    using BinaryExpression::BinaryExpression;

    int64_t Evaluate() override {
        return left_->Evaluate() * right_->Evaluate();
    }
};

class DivisionExpression : public BinaryExpression {
public:
    using BinaryExpression::BinaryExpression;

    int64_t Evaluate() override {
        return left_->Evaluate() / right_->Evaluate();
    }
};

std::unique_ptr<Expression> ParseTerm(Tokenizer* tok);

std::unique_ptr<Expression> ParseExpression(Tokenizer* tok) {
    auto term = ParseTerm(tok);
    while (tok->GetType() == Tokenizer::TokenType::kSymbol && (tok->GetSymbol() == '+' || tok->GetSymbol() == '-')) {
        char op = tok->GetSymbol();
        tok->Consume();
        auto nextTerm = ParseTerm(tok);
        if (op == '+') {
            term = std::make_unique<AdditionExpression>(std::move(term), std::move(nextTerm));
        } else if (op == '-') {
            term = std::make_unique<SubtractionExpression>(std::move(term), std::move(nextTerm));
        }
    }
    return term;
}

std::unique_ptr<Expression> ParseFactor(Tokenizer* tok) {
    if (tok->GetType() == Tokenizer::TokenType::kNumber) {
        int64_t value = tok->GetNumber();
        tok->Consume();
        return std::make_unique<ConstantExpression>(value);
    }
    if (tok->GetType() == Tokenizer::TokenType::kSymbol && tok->GetSymbol() == '-') {
        tok->Consume();
        auto factor = ParseFactor(tok);
        return std::make_unique<ConstantExpression>(-factor->Evaluate());
    }
    if (tok->GetType() == Tokenizer::TokenType::kSymbol && tok->GetSymbol() == '(') {
        tok->Consume();
        auto expr = ParseExpression(tok);
        if (tok->GetType() != Tokenizer::TokenType::kSymbol || tok->GetSymbol() != ')') {
            throw std::runtime_error("Mismatched parentheses");
        }
        tok->Consume();
        return expr;
    }
    throw std::runtime_error("Invalid expression");
}

std::unique_ptr<Expression> ParseTerm(Tokenizer* tok) {
    auto factor = ParseFactor(tok);
    while (tok->GetType() == Tokenizer::TokenType::kSymbol && (tok->GetSymbol() == '*' || tok->GetSymbol() == '/')) {
        char op = tok->GetSymbol();
        tok->Consume();
        auto nextFactor = ParseFactor(tok);
        if (op == '*') {
            factor = std::make_unique<MultiplicationExpression>(std::move(factor), std::move(nextFactor));
        } else if (op == '/') {
            factor = std::make_unique<DivisionExpression>(std::move(factor), std::move(nextFactor));
        }
    }
    return factor;
}
