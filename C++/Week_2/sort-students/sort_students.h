#pragma once

#include <vector>
#include <stdexcept>

struct Student {
    std::string name, surname;
    int year, month, day;

    Student() {}

    Student(std::string name, std::string surname, int year, int month, int day) :
          name(name), surname(surname), year(year), month(month), day(day) {}
};

enum class SortType { kByName, kByDate };

bool CompareByName(Student lhs, Student rhs) {
    if (lhs.surname != rhs.surname) {
        return lhs.surname < rhs.surname;
    } else if (lhs.name != rhs.name) {
        return lhs.name < rhs.name;
    } else if (lhs.year != rhs.year) {
        return lhs.year > rhs.year;
    } else if (lhs.month != rhs.month) {
        return lhs.month > rhs.month;
    }
    return lhs.day > rhs.day;
}

bool CompareByDate(Student lhs, Student rhs) {
    if (lhs.year != rhs.year) {
        return lhs.year > rhs.year;
    } else if (lhs.month != rhs.month) {
        return lhs.month > rhs.month;
    } else if (lhs.day != rhs.day) {
        return lhs.day > rhs.day;
    } else if (lhs.surname != rhs.surname) {
        return lhs.surname < rhs.surname;
    }
    return lhs.name < rhs.name;
}

inline void SortStudents(std::vector<Student> *students, SortType sort_type) {
    if (sort_type == SortType::kByName) {
        std::sort(students->begin(), students->end(), CompareByName);
    } else if (sort_type == SortType::kByDate) {
        std::sort(students->begin(), students->end(), CompareByDate);
    }
}
