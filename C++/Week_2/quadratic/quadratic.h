#pragma once

#include <stdexcept>

enum class RootCount { kZero, kOne, kTwo, kInf };

struct Roots {
    RootCount count;
    double first, second;
};

Roots SolveQuadratic(int a, int b, int c) {
    Roots ans;
    if (a == 0) {
        if (b == 0) {
            if (c == 0) {
                ans.count = RootCount::kInf;
                return ans;
            }
            ans.count = RootCount::kZero;
            return ans;
        }
        ans.count = RootCount::kOne;
        ans.first = -static_cast<double>(c) / b;
        return ans;
    }
    double discriminant = b * b - 4 * a * c;
    if (discriminant < 0) {
        ans.count = RootCount::kZero;
        return ans;
    } else if (discriminant == 0) {
        ans.count = RootCount::kOne;
        ans.first = -static_cast<double>(b) / 2 / a;
        return ans;
    }
    ans.count = RootCount::kTwo;
    ans.first = std::min((-b - std::sqrt(discriminant)) / 2 / a, (-b + std::sqrt(discriminant)) / 2 / a);
    ans.second = std::max((-b - std::sqrt(discriminant)) / 2 / a, (-b + std::sqrt(discriminant)) / 2 / a);
    return ans;
}
