#pragma once

#include <vector>
#include <stdexcept>

void FilterEven(std::vector<int> *data) {
    int odd_num = 0;
    for (auto it = data->begin(); it < data->end(); ++it) {
        if (*it % 2) {
            ++odd_num;
        } else {
            std::iter_swap(it, it - odd_num);
        }
    }
    while (odd_num--) {
        data->pop_back();
    }
}
