#pragma once

#include <iostream>
#include <utility>
#include <vector>
#include <stdexcept>

std::vector<std::pair<int64_t, int>> Factorize(int64_t x) {
    std::vector<std::pair<int64_t, int>> factor;
    //    std::cout << "start" << std::endl;
    int64_t divisor = 1;
    while (divisor * divisor < x) {
        ++divisor;
        int counter = 0;
        while (x % divisor == 0) {
            //            std::cout << divisor << " " << counter << " " << x << std::endl;
            x /= divisor;
            ++counter;
        }
        if (counter) {
            factor.emplace_back(divisor, counter);
        }
        //        std::cout << divisor << " " << counter << " " << x << std::endl;
    }
    if (x > 1) {
        factor.emplace_back(x, 1);
    }
    return factor;
}
