#pragma once

#include <stdexcept>
#include <vector>

void Rotate(std::vector<int> *data, size_t shift) {
//    throw std::runtime_error("Not implemented");
    //    for (size_t i = 0; i < std::max(shift, data->size() - shift); ++i) {
    std::reverse(data->begin(), data->end());
    std::reverse(data->begin(), data->begin() + data->size() - shift);
    std::reverse(data->begin() + data->size() - shift, data->end());
}
