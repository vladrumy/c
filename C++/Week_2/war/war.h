#pragma once

#include <array>
#include <queue>
#include <stdexcept>

enum Winner { kFirst, kSecond, kNone };

struct GameResult {
    Winner winner;
    int turn;
};

bool CompareCards(int lhs, int rhs) {
    if (lhs == 0) {
        return rhs != 9;
    } else if (lhs == 9) {
        return rhs == 0;
    }
    return lhs < rhs;
}

void SimulateTurn(std::queue<int>& first_deck, std::queue<int>& second_deck) {
    auto first_card = first_deck.front();
    first_deck.pop();
    auto second_card = second_deck.front();
    second_deck.pop();
    if (CompareCards(first_card, second_card)) {
        second_deck.push(first_card);
        second_deck.push(second_card);
    } else {
        first_deck.push(first_card);
        first_deck.push(second_card);
    }
}

GameResult SimulateWarGame(const std::array<int, 5>& first_deck,
                           const std::array<int, 5>& second_deck) {
    std::queue<int> first;
    for (auto elem : first_deck) {
        first.push(elem);
    }
    std::queue<int> second;
    for (auto elem : second_deck) {
        second.push(elem);
    }
    int counter = 0;
    while (counter < 1e6 && !first.empty() && !second.empty()) {
        SimulateTurn(first, second);
        ++counter;
    }
    GameResult result;
    result.turn = counter;
    if (second.empty()) {
        result.winner = Winner::kFirst;
    } else if (first.empty()) {
        result.winner = Winner::kSecond;
    } else {
        result.winner = Winner::kNone;
    }
    return result;
}
