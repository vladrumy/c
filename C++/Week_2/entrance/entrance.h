#pragma once

#include <stdexcept>
#include <map>
#include <string>
#include <vector>
#include <tuple>
#include <unordered_map>

struct StudentName {
    std::string name, surname;
};

struct Date {
    int year, month, day;
};

struct Student {
    std::string name, surname;
    int score, year, month, day;
    std::vector<std::string> universities;

    Student() {}

    Student(std::string name, std::string surname, int score, int year, int month, int day, std::vector<std::string> universities) :
          name(name), surname(surname), score(score), year(year), month(month), day(day), universities(universities) {}
};

enum class SortType { kByScore, kByName, kByDate };

bool CompareByScore(Student lhs, Student rhs) {
    if (lhs.score != rhs.score) {
        return lhs.score < rhs.score;
    } else if (lhs.year != rhs.year) {
        return lhs.year < rhs.year;
    } else if (lhs.month != rhs.month) {
        return lhs.month < rhs.month;
    } else if (lhs.day != rhs.day) {
        return lhs.day < rhs.day;
    } else if (lhs.surname != rhs.surname) {
        return lhs.surname < rhs.surname;
    }
    return lhs.name < rhs.name;
}

bool CompareByName(Student lhs, Student rhs) {
    if (lhs.surname != rhs.surname) {
        return lhs.surname < rhs.surname;
    } else if (lhs.name != rhs.name) {
        return lhs.name < rhs.name;
    } else if (lhs.year != rhs.year) {
        return lhs.year > rhs.year;
    } else if (lhs.month != rhs.month) {
        return lhs.month > rhs.month;
    }
    return lhs.day > rhs.day;
}

bool CompareByDate(Student lhs, Student rhs) {
    if (lhs.year != rhs.year) {
        return lhs.year > rhs.year;
    } else if (lhs.month != rhs.month) {
        return lhs.month > rhs.month;
    } else if (lhs.day != rhs.day) {
        return lhs.day > rhs.day;
    } else if (lhs.surname != rhs.surname) {
        return lhs.surname < rhs.surname;
    }
    return lhs.name < rhs.name;
}

inline void SortStudents(std::vector<Student> *students, SortType sort_type) {
    if (sort_type == SortType::kByScore) {
        std::sort(students->begin(), students->end(), CompareByScore);
    } else if (sort_type == SortType::kByName) {
        std::sort(students->begin(), students->end(), CompareByName);
    } else if (sort_type == SortType::kByDate) {
        std::sort(students->begin(), students->end(), CompareByDate);
    }
}

std::map<std::string, std::vector<StudentName>> GetStudents(
    const std::vector<std::pair<std::string, int>>& university_info,
    const std::vector<std::tuple<StudentName, Date, int, std::vector<std::string>>>&
        students_info) {
    std::unordered_map<std::string, int> universities_limits;
    for (auto& [name, limit] : university_info) {
        universities_limits[name] = limit;
    }
    std::vector<Student> students;
    for (auto& [name, date, score, universities] : students_info) {
        students.emplace_back(name.name, name.surname, score, date.year, date.month, date.day, universities);
    }
    SortStudents(&students, SortType::kByScore);
    std::map<std::string, std::vector<Student>> abiturients;
    for (auto student : students) {
        for (auto university : student.universities) {
            if (universities_limits[university]) {
                abiturients[university].push_back(student);
                --universities_limits[university];
            }
        }
    }
    for (auto& [university, university_abiturients] : abiturients) {
        SortStudents(&university_abiturients, SortType::kByName);
    }
}
