#include <catch.hpp>
#include <point_triangle.h>

TEST_CASE("Apex") {
    Triangle t(0, 0, 1, 0 ,0, 1);
    Point pt(0, 0);
    REQUIRE(true == IsPointInTriangle(t, pt));
}

TEST_CASE("Edge") {
    Triangle t(1, 1, 3, 5, 5, 3);
    Point pt(2, 3);
    REQUIRE(true == IsPointInTriangle(t, pt));
}

TEST_CASE("Inside") {
    Triangle t(1, 1, 3, 5, 5, 3);
    Point pt(3, 3);
    REQUIRE(true == IsPointInTriangle(t, pt));
}

TEST_CASE("Outside") {
    Triangle t(0, 0, 1, 0 ,0, 1);
    Point pt(1, 1);
    REQUIRE(false == IsPointInTriangle(t, pt));
}
