#pragma once

#include <stdexcept>
#include <iostream>

struct Point {
    int x, y;

    Point() {}

    Point(int x, int y) : x(x), y(y) {}
};

struct Triangle {
    Point a, b, c;

    Triangle() {}

    Triangle(int ax, int ay, int bx, int by, int cx, int cy) : a(ax, ay), b(bx, by), c(cx, cy) {}
};

struct Line {
    double a, b, c; // ay + bx + c = 0;
};

template <typename T> int Sign(T val) {
    return (T(0) < val) - (val < T(0));
}

inline Point PointDiff(const Point& a, const Point& b) {
    Point diff;
    diff.x = a.x - b.x;
    diff.y = a.y - b.y;
    return diff;
}

inline Line GetLine(const Point& a, const Point& b) {
    Line line;
    auto diff = PointDiff(a, b);
    if (diff.x == 0) {
        line.a = 0;
        line.b = -1;
        line.c = a.x;
        return line;
    }
    line.a = -1;
    line.b = static_cast<double>(diff.y) / diff.x;
    line.c = a.y - line.b * a.x;
    return line;
}

inline int ComparePointWithLine(const Point& a, const Point& b, const Point& c) {
    auto line = GetLine(b, c);
    return Sign(line.a * a.y + line.b * a.x + line.c);
}

inline bool IsPointOnSegment(const Point& a, const Point& b, const Point& c) {
    return (ComparePointWithLine(a, b, c) == 0 && Sign(PointDiff(a, c).x == Sign(PointDiff(b, c).x)));
}

inline bool IsPointInTriangle(const Triangle& t, const Point& pt) {
    if (IsPointOnSegment(pt, t.a, t.b)) {
        return true;
    } else if (IsPointOnSegment(pt, t.b, t.c)) {
        return true;
    } else if (IsPointOnSegment(pt, t.a, t.c)) {
        return true;
    } else if (ComparePointWithLine(pt, t.a, t.b) != ComparePointWithLine(t.c, t.a, t.b)) {
        return false;
    } else if (ComparePointWithLine(pt, t.b, t.c) != ComparePointWithLine(t.a, t.b, t.c)) {
        return false;
    } else if (ComparePointWithLine(pt, t.a, t.c) != ComparePointWithLine(t.b, t.a, t.c)) {
        return false;
    }
    return true;
}
